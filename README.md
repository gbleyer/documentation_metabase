Lien du schéma des principales tables de la base de donnée (très simplifié, uniquement avec les PK & FK): 
https://drive.google.com/file/d/1ol5UvDMovdYT7cxHiJrVA7RAOVpNjYUI/view?usp=sharing

## Variables:

Récupérer un ID dynamique:

	[...]
	WHERE companies.id = uuid({{id}})
	[...]

Trouver un résultat par l'intervalle de deux dates: 

	[...]
	AND created_at >= {{start_date}}
	AND created_at <= {{end_date}}
	[...]
	
---
## Companies:
---

**Nom du groupe/entreprise/agence/sous-traitant**:
companies.name

**ID du GROUPE**:
companies.headquarter_id 

**ID de l'entreprise/agence**:
companies.id

---

companies.roles = ''

**Tous les roles de companies**:

		contributor: 0,
		customer: 1,
		technician: 2

---

## Companies_contracts:

---

companies_contracts.gender = ''  

**Tous les gender du companies_contracts:**

		cc_gender_main_company: 0,
		cc_gender_working_company: 1,
		cc_gender_company: 2,
		cc_gender_customer: 3,
		cc_gender_technician: 4

---

companies_contracts.status = ""

**Tous les status du companies_contracts:**

		writing: 0,
		submitting: 1, #Not used anymore
		waiting: 2,
		correcting: 3,
		approving: 4,
		signing: 5,
		over: 6,
		canceled: 7,
		denied: 8,
		to_complete: 10,
		to_download: 11,
		deleted: 12,
		waiting_certification: 13
        
---

## Contracts:

contracts.categories = ''  

**Toutes les categories du contracts:**

		contract: "contract",
		approval: "approval",
		workorder: "workorder", #Not used anymore
		consortium: "consortium" #Not used anymore
	
        
---

contracts.markets = ''

**Tous les types de marchés du contracts:**

		public_market: 0,
        private_market: 1,
        individual_house_market: 2

---

## Requêtes:

---

**Companies_contracts** : Table qui fait la relation entre la table **Contracts** et la table **Companies** afin de récupérer tous les contrats d'une seule company.

0. (
    SELECT companies_contracts.id,companies_contracts.company_id AS company_id, companies_contracts.contract_id AS contract_id, companies_contracts.gender AS gender,
    companies_contracts.status AS status
    FROM companies_contracts, 
    **(SELECT id FROM companies WHERE companies.id = uuid({{id}}) ) AS children_companies /* variable qui permet de récupérer l'id de la company */**
    WHERE companies_contracts.company_id = children_companies.id
    ) AS cc

WHERE cc.company_id = companies.id 
AND contracts.id = cc.contract_id

---

**Friendships** : Table qui permet de récupérer tous les **Partenaires** liés à une company.

(
    SELECT DISTINCT friendships.company_id, friendships.related_company_id,  friendships.created_at, friendships.status FROM friendships,
    (SELECT id FROM companies where companies.headquarter_id  = uuid({{id}}) ) as children_company
    WHERE ( friendships.company_id = uuid(children_company.id)
    OR friendships.related_company_id = uuid(children_company.id) )
)  as f 

WHERE f.company_id = companies.id
AND f.status = 0 /* status : active, partenaires qui sont encore actifs sur le groupe */

---

**Companies_folders**: Table qui relie la table **Companies** et la table **Folders** afin de récupérer tous les folders d'une seule company.

(
    SELECT companies_folders.folder_id,  companies_folders.company_id FROM companies_folders,
    (SELECT id FROM companies WHERE companies.id = uuid({{id}}) ) AS children_companies
    WHERE companies_folders.company_id = children_companies.id
) AS cf

AND cf.folder_id = folders.id

---

**Companies_worksites**: Table qui lie la table **Companies** et la table **Worksites** dans le but de récupérer tous les worksites d'une seule company.

(
    SELECT companies_worksites.worksite_id, companies_worksites.company_id
    FROM companies_worksites, (SELECT id FROM companies WHERE companies.id = uuid({{id}}) ) AS children_companies
    WHERE companies_worksites.company_id = children_companies.id
) AS cw

WHERE cw.worksite_id = worksites.id

---

**Signatories**: Table qui permet de savoir la **date de signature d'un contrat**

(
    SELECT signatories.id, signatories.signed_at, signatories.company_id, signatories.contract_id 
    FROM signatories, (SELECT id FROM companies WHERE companies.id = uuid({{id}}) ) AS children_companies
    WHERE signatories.company_id = children_companies.id
    
) AS s

AND contracts.id = s.contract_id

---